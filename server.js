const _ = require('lodash')
const Koa = require('koa')
const Router = require('koa-router');
const bodyParser = require('koa-parser');

const app = new Koa();
const router = new Router();
const PORT = 4000;
const posts = [
  { id: 1, name: 'Nombre 1'},
  { id: 2, name: 'Nombre 2'},
  { id: 3, name: 'Nombre 3'},
  { id: 4, name: 'Nombre 4'},
]
app.use(bodyParser())

router.get('/', (ctx) => {
  ctx.body = 'Welcome to Koa Application!';
})

router.get('/posts/:id', ctx => {
  let id = parseInt(ctx.params.id)
  ctx.body = posts.find(post => post.id === id);
})

router.get('/posts', (ctx) => {
  ctx.body = posts;
})

router.post('/posts', (ctx) => {
  let { id, name } = ctx.request.body;
  if (!id) {
    ctx.throw(400, 'id is required')
  }
  if (!name) {
    ctx.throw(400, 'name is required')
  }
  posts.push({id, name});
  ctx.body = posts;
})

router.delete('/posts/:id', ctx => {
  let id = parseInt(ctx.params.id);
  ctx.body = _.remove(posts, p => p.id === id);
})

router.put('/posts/:id', ctx => {
  let { id, name } = ctx.request.body;
  const index = posts.findIndex(p => p.id === parseInt(ctx.params.id, 10));
  if (id) {
    posts[index].id = id;
  }
  if (name) {
    posts[index].name = name;
  }
  ctx.body = posts;
})




app.use(router.routes())

app.listen(PORT)
console.log(`Server is Listening on PORT http://localhost:${PORT}`)